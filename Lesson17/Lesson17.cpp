#include <iostream>
#include <math.h>

class Vector {

private:
    double x, y, z;

public:
    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}

    void Show() {
        std::cout << x << " " << y << " " << z << "\n";
    }

    double Lengtch() {
        return sqrt((x * x) + (y * y)  +  (z * z )); 
    }
};

int main()
{
    Vector v(10, 5, 3);

    v.Show(); 

    std::cout << v.Lengtch() << "\n";
}